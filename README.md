English:
Using this style sheet is completely free, you can modify the styles completely. This style template contains the essentials to create a good design, To install the style sheet you can follow the following steps
1.	The first step is to find the template in the place where you have saved it.
   * ![alt text](https://gitlab.com/JoseDaniel03/javafx-css/-/raw/master/Ignore/Add%20StyleSheet.jpg)
2.	After you have selected the style sheet, you can now start choosing the styles that you like.
   * ![alt text](https://gitlab.com/JoseDaniel03/javafx-css/-/raw/master/Ignore/SelectClass.jpg)
    

Spanish:
El uso de esta hoja de estilos es totalmente gratuito puedes modificar los estilos totalmente. Esta plantilla de estilos contiene lo esencial para crear un diseño bueno, Para instalar la hoja de estilos puedes seguir los siguientes pasos.
1.	El primer paso es buscar la plantilla en el sitio donde la hayas guardado
    * ![alt text](https://gitlab.com/JoseDaniel03/javafx-css/-/raw/master/Ignore/Add%20StyleSheet.jpg)
2.	Luego de haber seleccionado la hoja de estilos, ya puedes comenzar a elegir los estilos que tu gustes.
    * ![alt text](https://gitlab.com/JoseDaniel03/javafx-css/-/raw/master/Ignore/SelectClass.jpg)






Thank You !!
Gracias !!
